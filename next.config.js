
module.exports = {
    exportPathMap: async function (
        defaultPathMap,
        { dev, dir, outDir, distDir, buildId }
    ) {
        return {
            '/': { page: '/' },
            '/smalltv_01': { page: '/', query: { code: 0 }  },
            '/smalltv_02': { page: '/', query: { code: 1} },
            '/smalltv_03': { page: '/', query: { code: 2 } },
            '/bigtv': { page: '/', query: { code: 3 } },
            '/SmallTV_01': { page: '/', query: { code: 0 }  },
            '/SmallTV_02': { page: '/', query: { code: 1} },
            '/SmallTV_03': { page: '/', query: { code: 2 } },
            '/BigTV': { page: '/', query: { code: 3 } },
            '/filterpage': { page: '/pages/filterpage' },
        }
    },
}