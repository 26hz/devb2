import { useEffect, useState } from "react";

const PortraitLock = () => {
    const [isProtrait, setIsPortrait] = useState(typeof window !== "undefined" && window.orientation == 0);
    useEffect(() => {
        if (typeof window !== "undefined") {
            window.addEventListener("orientationchange", function(event) {
                if(window.orientation == 0 || window.orientation == 180) {
                    setIsPortrait(true)
                } else {
                    setIsPortrait(false)
                }
            })
        }
    }, [])

    if (!isProtrait) {
        return <div className="portrait-lock">
           Please lock your device to Portrait mode to continue<br/>請將您的設備鎖定為直立模式
        </div>
    }
    
    return null;
}

export default PortraitLock;