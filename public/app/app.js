var canvasHeight = window.innerHeight;
var canvasWidth = window.innerWidth;

var filterIndex = localStorage.getItem("filtercode");

var effects = [
  "/effects/chrown",
  "/effects/glass",
  "/effects/captain_hat",
  "/effects/heart_glass_chef_hat",
];

// desktop, the width of the canvas is 0.66 * window height and on mobile it's fullscreen
if (window.innerWidth > window.innerHeight) {
  canvasWidth = Math.floor(window.innerHeight * 0.66);
}

var deepAR = DeepAR({
  canvasWidth: canvasWidth,
  canvasHeight: canvasHeight,
  licenseKey:
    "046c4a82c7321ed44242926d4d085db0d75298797cc88045fd89d902559913cddb4e223affea579b",
  canvas: document.getElementById("deepar-canvas"),
  numberOfFaces: 1,
  libPath: "/lib",
  segmentationInfoZip: "segmentation.zip",
  onInitialize: function () {
    deepAR.startVideo(true);
    deepAR.switchEffect(0, "slot", effects[filterIndex]);
  
  },
});

function init() {
  
  const canvas = document.getElementById("deepar-canvas")
  const ctx = canvas.getContext('2d')
  const frame = document.getElementById('frame-overlay')
  ctx.drawImage(frame, 10, 10)
}

deepAR.onVideoStarted = function () {
};

deepAR.onCameraPermissionAsked = function () {
  console.log("camera permission asked");
};

deepAR.onCameraPermissionGranted = function () {
  console.log("camera permission granted");
};

deepAR.onCameraPermissionDenied = function () {
  console.log("camera permission denied");
};

deepAR.onImageVisibilityChanged = function (visible) {
  console.log("image visible", visible);
};

deepAR.onFaceVisibilityChanged = function (visible) {
  console.log("face visible", visible);
};

deepAR.downloadFaceTrackingModel("/lib/models-68-extreme.bin");

