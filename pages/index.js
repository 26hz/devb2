import React, { useState, useEffect, useRef } from "react";
import Head from "next/head";
import * as tmImage from "@teachablemachine/image";
import {
  isChrome,
  isSafari,
  isAndroid,
  isIOS,
  isMobile,
} from "react-device-detect";
import copy from "copy-to-clipboard";
import ReactLoading from "react-loading";
import QRCode from "react-qr-code";
import PortraitLock from "../components/PortraitLock";

const URL = "https://storage.googleapis.com/tm-model/gh4SD1u8v/";
const modelURL = URL + "model.json";
const metadataURL = URL + "metadata.json";

const imageMarker = ["TV"];

TeachableMachineTracking.getInitialProps = async (context) => {
  return { filterCode: context.query.code };
};

function TeachableMachineTracking({ filterCode }) {
  const [baseURL, setBaseURL] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const videoRef = useRef();
  let model, code;

  useEffect(() => {
    if (window.location.hostname === "visitvictoriaharbour.hk") {
      window.location.href = `https://www.visitvictoriaharbour.hk${window.location.pathname}`;
    }

    if (isMobile) {
      if (isIOS && !isSafari) {
        console.log("its mus open on safari");
        setIsLoading(false);
      } else if (isAndroid && !isChrome) {
        window.location.href = `googlechrome://navigate?url=${window.location.href}`;
      } else if ((isIOS && isSafari) || (isAndroid && isChrome)) {
        setIsLoading(false);
        _init();
      } else {
        setIsLoading(false);
      }
    } else {
      setIsLoading(false);
      setBaseURL(window.location.href);
    }
  }, []);

  const _init = async () => {
    localStorage.setItem("filtercode", filterCode);

    model = await tmImage.load(modelURL, metadataURL);
    let webcamStream = await navigator.mediaDevices.getUserMedia({
      video: {
        advanced: [
          {
            facingMode: "environment",
          },
        ],
      },
      audio: false,
    });

    videoRef.current.srcObject = webcamStream;
    window.stream = webcamStream;

    const delay = (s) => {
      return new Promise((resolve) => {
        setTimeout(resolve, s);
      });
    };

    await delay(2500);
    window.requestId = window.requestAnimationFrame(_loop);
  };

  const _loop = async () => {
    try {
      await _predict();
    } catch (err) {
      console.log(err);
    }
  };

  const _predict = async () => {
    if (videoRef.current.srcObject !== null) {
      const prediction = await model.predict(videoRef.current);
      let probabilities = {
        TV: 0.98,
      };

      prediction.forEach((val) => {
        if (
          imageMarker.includes(val.className) &&
          val.probability >= probabilities[val.className]
        ) {
          _stop();
          _triggerToFilterPage();
        }
      });
      window.requestAnimationFrame(_loop);
    }
  };

  const _stop = () => {
    window.cancelAnimationFrame(_loop);
    var stream = videoRef.current.srcObject;
    var tracks = stream.getTracks();

    for (var i = 0; i < tracks.length; i++) {
      var track = tracks[i];
      track.stop();
    }

    videoRef.current.srcObject = null;
  };

  const _triggerToFilterPage = () => {
    console.log(`Filter Code ${code}`);

    // Trigger Here. pass marker props on deepAr page, to trigger the filter
    window.location = `/filterpage`;
  };

  const _copyLink = () => {
    copy(window.location.href);
    alert("Link Copied");
  };

  return (
    <div>
      <Head>
        <title>Victoria Harbour AR</title>
      </Head>

      {isLoading ? (
        <div className="loading-container">
          <ReactLoading color="#3498db" height={"20%"} width={"20%"} />
        </div>
      ) : isMobile ? (
        (isIOS && isSafari) || (isAndroid && isChrome) ? (
          <React.Fragment>
            <PortraitLock />
            <main className="detect">
              <div className="header">
                <img
                  src="/assets/image/top-banner.png"
                  className="top-banner"
                />
              </div>
              <div className="video">
                <img
                  src="/assets/image/camera-frame.png"
                  className="camera-frame"
                />
                <video
                  id="camera-video"
                  muted
                  ref={videoRef}
                  autoPlay
                  playsInline={true}
                  style={{ width: "100%" }}
                  controls={false}
                />
              </div>
              <div className="footer">
                <p>將相機對準維多利亞港電視台</p>
                <p>Aim your camera at the Victoria Harbour TV</p>
              </div>
            </main>
          </React.Fragment>
        ) : (
          <div className="redirection">
            <img
              className="browser-logo"
              src={
                isIOS ? "/assets/image/safari.png" : "/assets/image/chrome.png"
              }
            />
            <div>
              <p className="my-3">
                {isIOS
                  ? "Open with Safari for iOS to access this content"
                  : "Open with Chrome for Android to access this content"}
              </p>
              <p className="mb-2">
                Tap below to copy the address for easy pasting into{" "}
                {isIOS ? "Safari for iOS" : "Chrome for Android"}
              </p>
              <button className="btn" onClick={_copyLink}>
                Copy Page Link
              </button>
            </div>
          </div>
        )
      ) : (
        <div className="dekstop-container">
          <div className="barcode-container">
            <QRCode value={`${baseURL}smalltv_01`} />
          </div>
          <h2>
            To view, open camera on <br /> smartphone and scan code
          </h2>
          <p>or visit</p>
          <input className="input-link" value={`${baseURL}smalltv_01`} />
          <p>on a smartphone or tablet</p>
        </div>
      )}
    </div>
  );
}

export default TeachableMachineTracking;
