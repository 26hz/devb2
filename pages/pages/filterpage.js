import React, { useState, useEffect, useRef } from "react";
import Head from "next/head";
import styles from "../../styles/Home.module.css";
import PortraitLock from '../../components/PortraitLock';


const overlayImage = [
  "/assets/image/Filter-frame/frame2.png",
  "/assets/image/Filter-frame/frame4.png",
  "/assets/image/Filter-frame/frame1.png",
  "/assets/image/Filter-frame/frame3.png",
];

let context, canvas, dpi;

export default function Home() {
  const [isUploading, setIsUploading] = useState(false);
  const [isPreview, setIsPreview] = useState(false);
  const [imageLink, setImageLink] = useState("");
  const [code, setCode] = useState();

  useEffect(() => {
    dpi = window.devicePixelRatio;

    _isStarting();
    canvas = document.getElementById("viewport");

    context = canvas.getContext("2d");
  }, []);

  const _isStarting = async () => {
    const filterIndex = localStorage.getItem("filtercode");
    setCode(filterIndex);
  };

  const _takeSceenShoot = async () => {
    window.deepAR.takeScreenshot();
    window.deepAR.onScreenshotTaken = async function (photo) {
      const img = new Image();
      img.onload = function () {
        canvas.setAttribute("height", img.height * dpi);
        canvas.setAttribute("width", img.width * dpi);

        const image2 = new Image();
        const image1 = new Image();

        image2.src = overlayImage[code];

        image2.onload = function () {
          image1.onload = function () {
            context.drawImage(image1, 0, 0, canvas.width, canvas.height);
            context.globalAlpha = 1; //Remove if pngs have alpha
            context.drawImage(image2, 0, 0, canvas.width, canvas.height);
            const dataURL = canvas.toDataURL("image/jpeg", 1);
            setIsPreview(true);
            setImageLink(dataURL);
          };
          image1.src = photo;
        };
      };

      img.src = photo;
    };
  };

  const _closePreview = () => {
    setIsPreview(false);
    window.location.reload();
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Victoria Harbour AR</title>
      </Head>
      <PortraitLock />
      <main style={{ height: "100vh", width: "100%", overflow: "hidden" }}>
        {!isPreview ? (
          <div style={{ display: "flex" }}>
            <div id="sc">
              <img
                className="frame-overlay"
                id="frame-overlay"
                src={overlayImage[code]}
              />
              <canvas
                className="deepar"
                id="deepar-canvas"
                style={{ overflow: "hidden" }}
              />
              <canvas id="viewport" className="viewport" />
            </div>
            <div className="take-button" onClick={_takeSceenShoot}>
              <div />
            </div>
          </div>
        ) : (
          <div>
            <div className="img-prev">
              <p
                style={{
                  position: "absolute",
                  left: 20,
                  fontSize: 25,
                  fontWeight: "bold",
                  color: "white",
                }}
                onClick={_closePreview}
              >
                X
              </p>
              <img src={imageLink} className="img" />
              <div
                style={{
                  bottom: 30,
                  position: "absolute",
                  borderRadius: 5,
                  alignItems: "center",
                  width: "100%",
                  padding: 7,
                }}
              >
                <div
                  style={{
                    fontWeight: "bold",
                    color: "white",
                    textAlign: "center",
                  }}
                >
                  長按相片收藏並與家人朋友分享吧！
                  <br />
                  Long press the photo to save and share with your family and
                  friends!
                </div>
              </div>
            </div>
          </div>
        )}
        <script type="text/javascript" src="/lib/deepar.js"></script>
        <script type="text/javascript" src="/app/app.js"></script>
      </main>
    </div>
  );
}
